<?php

return array(

	/**
	 * Consumer key provided by Yahoo!
	 */
	'consumer_key' => '',

	/**
	 * Consumer secret key provided by Yahoo!
	 */
	'consumer_secret' => '',

	/**
	 * Application ID provided by Yahoo!
	 */
	'application_id' => ''

);