<?php

return array(

	/**
	 * Consumer key provided by Twitter
	 */
	'consumer_key' => '',

	/**
	 * Consumer secret key provided by Twitter
	 */
	'consumer_secret' => ''

);