<?php namespace Cerbero\Oauth;

/**
 * Interface for OAuth.
 *
 * @author	Andrea Marco Sartori
 */
interface OAuthInterface
{

	/**
	 * Generate an authorization URL with the given scopes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string
	 */
	public function request();

	/**
	 * Store the provider tokens.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function authorize();

	/**
	 * Return the provider client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function client();

	/**
	 * Retrieve the user identifier given by the provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function id();

	/**
	 * Retrieve the user email used with the provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string|UnexpectedValueException
	 */
	public function email();

}