<?php

Route::filter('csrf.oauth', function($route)
{
	$provider = $route->getParameter('provider');

	$exclude = array('twitter', 'yahoo');

	if ( ! in_array($provider, $exclude) && Session::token() != Input::get('state'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});