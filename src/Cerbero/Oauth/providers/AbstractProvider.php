<?php namespace Cerbero\Oauth\Providers;

/**
 * Abstraction of an OAuth provider.
 *
 * @author	Andrea Marco Sartori
 */
abstract class AbstractProvider implements ProviderInterface
{

	/**
	 * Retrieve the redirect URI.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string
	 */
	protected function getRedirectUri()
	{
		$provider = $this->getName();

		return url("login/oauth/{$provider}");
	}

	/**
	 * Retrieve the provider name.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string
	 */
	protected function getName()
	{
		$name = get_called_class();

		$segments = explode('\\', $name);

		return strtolower(end($segments));
	}

	/**
	 * Call services dynamically.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$name
	 * @param	array|null	$arguments
	 * @return	mixed
	 */
	public function hookService($name, $arguments = null)
	{
		$provider = $this->getName();

		$service = app("oauth.{$provider}.services.{$name}");

		$service->setAttributes($arguments);

		return $service;
	}

	/**
	 * Retrieve the authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array|null	$scopes
	 * @return	string
	 */
	abstract public function getAuthUrl($scopes = null);

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	abstract public function storeAccessToken();

	/**
	 * Return the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	abstract public function getClient();

	/**
	 * Retrieve the unique user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	abstract public function getID();

	/**
	 * Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string|UnexpectedValueException
	 */
	abstract public function getEmail();

}