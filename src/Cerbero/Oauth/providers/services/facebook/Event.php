<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

/**
 * Service for events.
 *
 * @author	Andrea Marco Sartori
 */
class Event extends AbstractFacebookService
{

	/**
	 * Retrieve an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function get()
	{
		if($this->getAttributes())
		{
			return parent::get();
		}
		return $this->api('me/events');
	}

	/**
	 * Create an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$params
	 * @return	int
	 */
	public function create($params)
	{
		$id = head($this->api('me/events', 'POST', $params));

		$this->attachMedia($id);

		return $id;
	}

	/**
	 * Update an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$params
	 * @return	boolean
	 */
	public function update($params)
	{
		return parent::update($params);
	}

	/**
	 * Remove an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @return	int
	 */
	public function remove()
	{
		return parent::remove();
	}

	/**
	 * Retrieve attenders.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function attenders()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/attending");
	}

	/**
	 * Attend an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function attend()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/attending", 'POST');
	}

	/**
	 * Retrieve decliners.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function decliners()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/declined");
	}

	/**
	 * Decline an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function decline()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/declined", 'POST');
	}

	/**
	 * Retrieve invited.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function invited()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/invited");
	}

	/**
	 * Invite people.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function invite()
	{
		$args = is_array($arr = func_get_arg(0)) ? $arr : func_get_args();

		$users = implode(',', $args);

		$id = $this->getAttributes();

		return $this->api("{$id}/invited", 'POST', compact('users'));
	}

	/**
	 * Exclude people.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function exclude()
	{
		$id = $this->getAttributes();

		$users = is_array($arr = func_get_arg(0)) ? $arr : func_get_args();

		return $this->massExclude($users, $id);
	}

	/**
	 * Exclude an array of IDs.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$users
	 * @param	int		$id
	 * @return	boolean
	 */
	protected function massExclude(array $users, $id)
	{
		$boolean = true;

		foreach ($users as $user)
		{
			$boolean = $this->api("{$id}/invited", 'DELETE', compact('user')) && $boolean;
		}
		return $boolean;
	}

	/**
	 * Retrieve all posts.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function posts()
	{
		return parent::posts();
	}

	/**
	 * Add a post to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$params
	 * @return	int
	 */
	public function post($params)
	{
		return parent::post($params);
	}

	/**
	 * Retrieve people who replied with maybe.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function maybes()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/maybe");
	}

	/**
	 * Reply with maybe.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function maybe()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/maybe", 'POST');
	}

	/**
	 * Retrieve people who haven't replied yet.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function noreplies()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/noreply");
	}

	/**
	 * Retrieve the picture.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$type
	 * @param	boolean	$redirect
	 * @return	mixed
	 */
	public function picture($type, $redirect = true)
	{
		return parent::picture($type, $redirect);
	}

	/**
	 * Set the picture.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$param
	 * @return	AbstractFacebookService
	 */
	public function setPicture($param)
	{
		return parent::setPicture($param);
	}

	/**
	 * Retrieve all photos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function photos()
	{
		return parent::photos();
	}

	/**
	 * Add a photo.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$message
	 * @return	int
	 */
	public function addPhoto($source, $message = '')
	{
		return parent::addPhoto($source, $message);
	}

	/**
	 * Allow media addition via method chaining.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$title
	 * @param	string	$description
	 * @return	Event
	 */
	public function with($source, $title = '', $description = '')
	{
		return parent::with($source, $title, $description);
	}

	/**
	 * Retrieve all videos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function videos()
	{
		return parent::videos();
	}

	/**
	 * Add a video.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$title
	 * @param	string	$description
	 * @return	int
	 */
	public function addVideo($source, $title = '', $description = '')
	{
		return parent::addVideo($source, $title, $description);
	}

}