<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

/**
 * Service for checkins.
 *
 * @author	Andrea Marco Sartori
 */
class Checkin extends AbstractFacebookService
{

	/**
	 * Retrieve a checkin.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function get()
	{
		return parent::get();
	}

	/**
	 * Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function likes()
	{
		return parent::likes();
	}

	/**
	 * Like a checkin.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	public function like()
	{
		return parent::like();
	}

	/**
	 * Dislike a checkin.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	public function dislike()
	{
		return parent::dislike();
	}

	/**
	 * Retrieve all comments.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function comments()
	{
		return parent::comments();
	}

	/**
	 * Add a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @return	int
	 */
	public function comment($message)
	{
		return parent::comment($message);
	}

}