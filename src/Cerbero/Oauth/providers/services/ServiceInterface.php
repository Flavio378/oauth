<?php namespace Cerbero\Oauth\Providers\Services;

/**
 * Service interface.
 *
 * @author	Andrea Marco Sartori
 */
interface ServiceInterface
{

	/**
	 * Set the service attributes.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$attributes
	 * @return	void
	 */
	public function setAttributes($attributes);

}