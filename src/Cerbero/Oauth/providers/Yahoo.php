<?php namespace Cerbero\Oauth\Providers;

use Cerbero\Oauth\Storage\TokenStorageInterface;

/**
 * Yahoo! provider.
 *
 * @author	Andrea Marco Sartori
 */
class Yahoo extends AbstractProvider
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		\YahooOAuthApplication	$client	Yahoo! client.
	 */
	protected $client;

	/**
	 * @author	Andrea Marco Sartori
	 * @var		Cerbero\Oauth\Storage\TokenStorageInterface	$storage	Storage for token.
	 */
	protected $storage;

	/**
	 * Set the dependencies.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	\YahooOAuthApplication	$client
	 * @param	Cerbero\Oauth\Storage\TokenStorageInterface	$client
	 * @return	void
	 */
	public function __construct(\YahooOAuthApplication $client, TokenStorageInterface $storage)
	{
		$this->client = $client;

		$this->storage = $storage;

		$this->setAccessToken();
	}

	/**
	 * Set the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function setAccessToken()
	{
		$accessToken = $this->storage->get('yahoo.access');

		$this->client->token = $accessToken;
	}

	/**
	 * Retrieve the authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array|null	$scopes
	 * @return	string
	 */
	public function getAuthUrl($scopes = null)
	{
		$requestToken = $this->getRequestToken();

		return $this->client->getAuthorizationUrl($requestToken);
	}

	/**
	 * Retrieve the request token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	YahooOAuthRequestToken
	 */
	protected function getRequestToken()
	{
		if( ! $this->storage->get('yahoo.request'))
		{
			$this->storeRequestToken();
		}
		return $this->storage->get('yahoo.request');
	}

	/**
	 * Store the request token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function storeRequestToken()
	{
		$callback = $this->getRedirectUri();

		$requestToken = $this->client->getRequestToken($callback);

		$this->storage->set('yahoo.request', $requestToken);
	}

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function storeAccessToken()
	{
		$accessToken = $this->getAccessToken();

		$this->storage->remove('yahoo.request');

		$this->storage->set('yahoo.access', $accessToken);
	}

	/**
	 * Retrieve the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	YahooOAuthAccessToken
	 */
	protected function getAccessToken()
	{
		$requestToken = $this->storage->get('yahoo.request');

		$verifier = \Input::get('oauth_verifier');

		return $this->client->getAccessToken($requestToken, $verifier);
	}

	/**
	 * Return the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	YahooOAuthApplication
	 */
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * Retrieve the unique user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getID()
	{
		return $this->client->getGUID();
	}

	/**
	 * Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string|UnexpectedValueException
	 */
	public function getEmail()
	{
		$email = $this->getPrimaryEmail();

		return $email->handle;
	}

	/**
	 * Retrieve the primary user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function getPrimaryEmail()
	{
		$emails = $this->client->getProfile()->profile->emails;

		return array_first($emails, function($key, $email)
		{
			return isset($email->primary);
		});
	}

}