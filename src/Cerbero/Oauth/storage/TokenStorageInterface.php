<?php namespace Cerbero\Oauth\Storage;

/**
 * Interface for token storage.
 *
 * @author	Andrea Marco Sartori
 */
interface TokenStorageInterface
{

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$providerName
	 * @param	string	$accessToken
	 * @return	void
	 */
	public function set($providerName, $accessToken);

	/**
	 * Retrieve the stored access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$providerName
	 * @return	string
	 */
	public function get($providerName);

	/**
	 * Remove the stored access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$providerName
	 * @return	void
	 */
	public function remove($providerName);

}