<?php namespace Cerbero\Oauth\Storage;

/**
 * Handler for token storage.
 *
 * @author	Andrea Marco Sartori
 */
class TokenStorage implements TokenStorageInterface
{

	/**
	 * Set the storage driver.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	Cerbero\Oauth\Storage\StorageInterface	$storage
	 * @return	void
	 */
	public function __construct(StorageInterface $storage)
	{
		$this->storage = $storage;
	}

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$providerName
	 * @param	string	$accessToken
	 * @return	void
	 */
	public function set($providerName, $accessToken)
	{
		$key = $this->getKey($providerName);

		$this->storage->put($key, $accessToken);
	}

	/**
	 * Retrieve the stored access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$providerName
	 * @return	string
	 */
	public function get($providerName)
	{
		$key = $this->getKey($providerName);

		return $this->storage->get($key);
	}

	/**
	 * Remove the stored access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$providerName
	 * @return	void
	 */
	public function remove($providerName)
	{
		$key = $this->getKey($providerName);

		$this->storage->forget($key);
	}

	/**
	 * Retrieve the key to store the tokens in.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$providerName
	 * @return	string
	 */
	protected function getKey($providerName)
	{
		return 'oauth.tokens.' . $providerName;
	}

}