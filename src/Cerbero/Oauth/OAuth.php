<?php namespace Cerbero\Oauth;

use Cerbero\Oauth\Providers\AbstractProvider;

/**
 * Class leveraging OAuth protocol.
 *
 * @author	Andrea Marco Sartori
 */
class OAuth implements OAuthInterface
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		ProviderInterface	$provider	Provider to perform OAuth.
	 */
	protected $provider;

	/**
	 * Set dependencies.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	Cerbero\Oauth\Providers\AbstractProvider	$provider
	 * @return	void
	 */
	public function __construct(AbstractProvider $provider)
	{
		$this->provider = $provider;
	}

	/**
	 * Call provider services dynamically.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	mixed
	 */
	public function __call($name, $arguments)
	{
		return $this->provider->hookService($name, $arguments);
	}

	/**
	 * Generate an authorization URL with the given scopes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string
	 */
	public function request()
	{
		$scopes = $this->getScopes(func_get_args());

		return $this->provider->getAuthUrl($scopes);
	}

	/**
	 * Retrieve the requested scopes.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$args
	 * @return	array|null
	 */
	protected function getScopes($args)
	{
		if(empty($args)) return null;

		return is_array($arr = $args[0]) ? $arr : $args;
	}

	/**
	 * Store the provider tokens.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function authorize()
	{
		$this->provider->storeAccessToken();
	}

	/**
	 * Return the provider client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function client()
	{
		return $this->provider->getClient();
	}

	/**
	 * Retrieve the user identifier given by the provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function id()
	{
		return $this->provider->getID();
	}

	/**
	 * Retrieve the user email used with the provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string|UnexpectedValueException
	 */
	public function email()
	{
		return $this->provider->getEmail();
	}

}