<?php

use \Mockery as m,
	Cerbero\Oauth\Storage\TokenStorage;

/**
 * Tests about storage.
 *
 * @author	Andrea Marco Sartori
 */
class TokenStorageTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$this->storage = m::mock('Cerbero\Oauth\Storage\StorageInterface');
	}

	/**
	 * @testdox	Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testStoreTheAccessToken()
	{
		$storage = $this->storage->shouldReceive('put')->once()->with('oauth.tokens.google', 1234)->mock();

		$tokenStorage = new TokenStorage($storage);

		$tokenStorage->set('google', 1234);
	}

	/**
	 * @testdox	Retrieve the stored access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheStoredAccessToken()
	{
		$storage = $this->storage->shouldReceive('get')->once()->with('oauth.tokens.google')->mock();

		$tokenStorage = new TokenStorage($storage);

		$tokenStorage->get('google');
	}

	/**
	 * @testdox	Remove the stored access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRemoveTheStoredAccessToken()
	{
		$storage = $this->storage->shouldReceive('forget')->once()->with('oauth.tokens.google')->mock();

		$tokenStorage = new TokenStorage($storage);

		$tokenStorage->remove('google');
	}

}