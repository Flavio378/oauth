<?php

use Mockery as m,
	Cerbero\Oauth\Providers\Twitter;

/**
 * Tests about Twitter provider.
 *
 * @author	Andrea Marco Sartori
 */
class TwitterTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$config = array
		(
			'user_agent' => 'Cerbero OAuth',

			'consumer_key' => m::any(),

			'consumer_secret' => m::any(),

			'token' => m::any(),

			'secret' => m::any()
		);
		
		$this->client = m::mock('TwitterOAuth');
                 
		$this->storage = m::mock('Cerbero\Oauth\Storage\TokenStorageInterface');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * @testdox	Generate authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testGenerateAuthorizationURL()
	{
		$callback = array('oauth_callback' => url('login/oauth/twitter'));

		$tokens = array('oauth_token' => 1234, 'oauth_token_secret' => 4321, 'oauth_callback_confirmed' => true);

		$client = $this->client->shouldReceive('getRequestToken')->once()->with($callback)->andReturn($tokens)->mock()
							   ->shouldReceive('getAuthorizeURL')->once()->with($tokens)->mock();

		$storage = $this->storage->shouldReceive('set')->once()->with('twitter.temp', $tokens)->mock();

		$twitter = new Twitter($client, $storage);

		$twitter->getAuthUrl();
	}

	/**
	 * @testdox	Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testStoreTheAccessToken()
	{
		$tokens = array('oauth_token' => 1234, 'oauth_token_secret' => 4321);

		$client = $this->client->shouldReceive('setOAuthToken')->once()->with(m::any(), 4321)->mock()
							   ->shouldReceive('getAccessToken')->once()->with(m::any())->andReturn($tokens)->mock();

		$storage = $this->storage->shouldReceive('get')->once()->with('twitter.temp')->andReturn($tokens)->mock()
								 ->shouldReceive('set')->once()->with('twitter', $tokens)->mock();

		$twitter = new Twitter($client, $storage);

		$twitter->storeAccessToken();
	}

	/**
	 * @testdox	Retrieve the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheClient()
	{
		$twitter = new Twitter($this->client, $this->storage);

		$client = $twitter->getClient();

		$this->assertInstanceOf('TwitterOAuth', $client);
	}

	/**
	 * @testdox	Retrieve the user ID.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserID()
	{
		$return = array('oauth_token' => 1234, 'oauth_token_secret' => 4321, 'user_id' => 22, 'screen_name' => 'username');

		$storage = $this->storage->shouldReceive('get')->once()->with('twitter')->andReturn($return)->mock();

		$twitter = new Twitter($this->client, $storage);

		$id = $twitter->getID();

		$this->assertSame(22, $id);
	}

	/**
	 * @testdox	Throw exception when username is requested.
	 * @expectedException UnexpectedValueException
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testThrowExceptionWhenUsernameIsRequested()
	{
		$twitter = new Twitter($this->client, $this->storage);

		$twitter->getEmail();
	}

}