<?php

use \Mockery as m,
	Cerbero\Oauth\Storage\LaravelStorage as Storage;

/**
 * Tests about Laravel drivers used as storage.
 *
 * @author	Andrea Marco Sartori
 */
class LaravelStorageTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();
		
		$this->driver = m::mock('Illuminate\Session\Store');
	}

	/**
	 * @testdox	Store a value in the given key.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testStoreAValueInTheGivenKey()
	{
		$driver = $this->driver->shouldReceive('put')->once()->with('key', 1234)->mock();

		$session = new Storage($driver);

		$session->put('key', 1234);
	}

	/**
	 * @testdox	Retrieve a value stored in the given key.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAValueStoredInTheGivenKey()
	{
		$driver = $this->driver->shouldReceive('get')->once()->with('key')->mock();

		$session = new Storage($driver);

		$session->get('key');
	}

	/**
	 * @testdox	Remove a value stored in the given key.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRemoveAValueStoredInTheGivenKey()
	{
		$driver = $this->driver->shouldReceive('forget')->once()->with('key')->mock();

		$session = new Storage($driver);

		$session->forget('key');
	}

}