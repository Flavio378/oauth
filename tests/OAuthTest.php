<?php

use \Mockery as m,
	Cerbero\Oauth\Oauth;

/**
 * Tests about OAuth class.
 *
 * @author	Andrea Marco Sartori
 */
class OAuthTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$this->provider = m::mock('Cerbero\Oauth\Providers\AbstractProvider');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * @testdox	Retrieve an authorization request with the given scope.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSendAnAuthorizationRequest()
	{
		$provider = $this->provider->shouldReceive('getAuthUrl')->once()->with(array('scope1'))->mock();

		$oauth = new Oauth($provider);

		$oauth->request('scope1');
	}

	/**
	 * @testdox	Multiple scopes can be set at once.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testMultipleScopesCanBeSetAtOnce()
	{
		$provider = $this->provider->shouldReceive('getAuthUrl')->once()->with(array('scope1', 'scope2'))->mock();

		$oauth = new Oauth($provider);

		$oauth->request('scope1', 'scope2');
	}

	/**
	 * @testdox	Multiple scopes can be set by array.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testMultipleScopesCanBeSetByArray()
	{
		$provider = $this->provider->shouldReceive('getAuthUrl')->once()->with(array('scope1', 'scope2'))->mock();

		$oauth = new Oauth($provider);

		$oauth->request(array('scope1', 'scope2'));
	}

	/**
	 * @testdox	Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testStoreTheAccessToken()
	{
		$provider = $this->provider->shouldReceive('storeAccessToken')->once()->mock();

		$oauth = new Oauth($provider);

		$oauth->authorize();
	}

	/**
	 * @testdox	Return the provider client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testReturnTheProviderClient()
	{
		$provider = $this->provider->shouldReceive('getClient')->once()->mock();

		$oauth = new Oauth($provider);

		$oauth->client();
	}

	/**
	 * @testdox	Retrieve the unique user identifier of the provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUniqueUserIdentifierOfTheProvider()
	{
		$provider = $this->provider->shouldReceive('getID')->once()->mock();

		$oauth = new Oauth($provider);

		$oauth->id();
	}

	/**
	 * @testdox	Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserEmail()
	{
		$provider = $this->provider->shouldReceive('getEmail')->once()->andReturn('test@example.com')->mock();

		$oauth = new Oauth($provider);

		$oauth->email();
	}

	/**
	 * @testdox	Call provider services dynamically.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCallProviderServicesDynamically()
	{
		$return = m::mock(array('returnValue' => 'test'));

		$this->provider->shouldReceive('hookService')->once()->with('service', null)->andReturn($return)->mock();

		$oauth = new Oauth($this->provider);

		$value = $oauth->service()->returnValue('test');

		$this->assertSame('test', $value);
	}

}