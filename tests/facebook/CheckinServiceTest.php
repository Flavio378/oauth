<?php

use Mockery as m;

/**
 * Tests about Facebook checkins.
 *
 * @author	Andrea Marco Sartori
 */
class CheckinServiceTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$this->client = m::mock('Cerbero\Oauth\Providers\Clients\Facebook');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * Bind the checkin service.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function bindCheckin()
	{
		$this->bindService('checkin', 'facebook');
	}

	/**
	 * @testdox	Callable by facade.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCallableByFacade()
	{
		$checkin = Facebook::checkin();

		$service = 'Cerbero\Oauth\Providers\Services\Facebook\Checkin';

		$this->assertInstanceOf($service, $checkin);
	}

	/**
	 * @testdox	Retrieve a checkin.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveACheckin()
	{
		$this->client->shouldReceive('api')->once()->with(5, 'GET', array());

		$this->bindCheckin();

		Facebook::checkin(5)->get();
	}

	/**
	 * @testdox	Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllLikes()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'GET', array());

		$this->bindCheckin();

		Facebook::checkin(5)->likes();
	}

	/**
	 * @testdox	Like a checkin.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testLikeACheckin()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'POST', array());

		$this->bindCheckin();

		Facebook::checkin(5)->like();
	}

	/**
	 * @testdox	Disike a checkin.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testDislikeACheckin()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'DELETE', array());

		$this->bindCheckin();

		Facebook::checkin(5)->dislike();
	}

	/**
	 * @testdox	Retrieve all comments.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllComments()
	{
		$this->client->shouldReceive('api')->once()->with('5/comments', 'GET', array());

		$this->bindCheckin();

		Facebook::checkin(5)->comments();
	}

	/**
	 * @testdox	Add a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAComment()
	{
		$this->client->shouldReceive('api')->once()->with('5/comments', 'POST', array('message' => 'comment'))->andReturn(array('id' => 6));

		$this->bindCheckin();

		$id = Facebook::checkin(5)->comment('comment');

		$this->assertSame(6, $id);
	}

}