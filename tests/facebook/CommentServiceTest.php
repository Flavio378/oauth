<?php

use Mockery as m;

/**
 * Tests about Facebook comments.
 *
 * @author	Andrea Marco Sartori
 */
class CommentServiceTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$this->client = m::mock('Cerbero\Oauth\Providers\Clients\Facebook');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * Bind the comment service.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function bindComment()
	{
		$this->bindService('comment', 'facebook');
	}

	/**
	 * @testdox	Callable by facade.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCallableByFacade()
	{
		$comment = Facebook::comment();

		$service = 'Cerbero\Oauth\Providers\Services\Facebook\Comment';

		$this->assertInstanceOf($service, $comment);
	}

	/**
	 * @testdox	Retrieve a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAComment()
	{
		$this->client->shouldReceive('api')->once()->with(5, 'GET', array());

		$this->bindComment();

		Facebook::comment(5)->get();
	}

	/**
	 * @testdox	Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllLikes()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'GET', array());

		$this->bindComment();

		Facebook::comment(5)->likes();
	}

	/**
	 * @testdox	Like a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testLikeAComment()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'POST', array());

		$this->bindComment();

		Facebook::comment(5)->like();
	}

	/**
	 * @testdox	Disike a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testDislikeAComment()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'DELETE', array());

		$this->bindComment();

		Facebook::comment(5)->dislike();
	}

	/**
	 * @testdox	Remove a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRemoveAComment()
	{
		$this->client->shouldReceive('api')->once()->with('5', 'DELETE', array())->andReturn(true);

		$this->bindComment();

		$boolean = Facebook::comment(5)->remove();

		$this->assertTrue($boolean);
	}

}