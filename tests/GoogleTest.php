<?php

use \Mockery as m,
	Cerbero\Oauth\Providers\Google;

/**
 * Tests about Google provider.
 *
 * @author	Andrea Marco Sartori
 */
class GoogleTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();
		
		$this->client = m::mock('Google_Client')->shouldReceive('setClientId', 'setClientSecret', 'setRedirectUri', 'setState')->once()->mock();

		$this->storage = m::mock('Cerbero\Oauth\Storage\TokenStorageInterface');

		$this->config = array('client_id' => 1, 'client_secret' => 2);
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * @testdox	Generate authorization URL with scopes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testGenerateAuthorizationURLWithScopes()
	{
		$client = $this->client->shouldReceive('setScopes')->once()->with(array('email', 'plus.me'))->mock()
							   ->shouldReceive('createAuthUrl')->once()->mock();

		$google = new Google($client, $this->storage, $this->config);

		$google->getAuthUrl(array('email', 'plus.me'));
	}

	/**
	 * @testdox	Access token is got.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAccessTokenIsGot()
	{
		$client = $this->client->shouldReceive('authenticate')->once()->mock();

		$storage = $this->storage->shouldReceive('set')->once()->mock();

		$google = new Google($client, $storage, $this->config);

		$google->storeAccessToken();
	}

	/**
	 * @testdox	Client is returned.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testClientIsReturned()
	{
		$google = new Google($this->client, $this->storage, $this->config);

		$client = $google->getClient();

		$this->assertInstanceOf('Google_Client', $client);
	}

	/**
	 * @testdox	Retrieve the user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserIdentifier()
	{
		$attributes = array('payload' => array('sub' => 1234));

		$ticket = m::mock('Google_Auth_LoginTicket')->shouldReceive('getAttributes')->once()->andReturn($attributes)->mock();

		$client = $this->client->shouldReceive('setAccessToken', 'verifyIdToken')->once()->andReturn($ticket)->mock();

		$storage = $this->storage->shouldReceive('get')->once()->with('google');

		$google = new Google($client, $this->storage, $this->config);

		$id = $google->getID();

		$this->assertSame(1234, $id);
	}

	/**
	 * @testdox	Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserEmail()
	{
		$attributes = array('payload' => array('email' => 'test@example.com'));

		$ticket = m::mock('Google_Auth_LoginTicket')->shouldReceive('getAttributes')->once()->andReturn($attributes)->mock();

		$client = $this->client->shouldReceive('setAccessToken', 'verifyIdToken')->once()->andReturn($ticket)->mock();

		$storage = $this->storage->shouldReceive('get')->once()->with('google');

		$google = new Google($client, $this->storage, $this->config);

		$email = $google->getEmail();

		$this->assertSame('test@example.com', $email);
	}

	/**
	 * @testdox	Throw exception if email is not provided.
	 * @expectedException UnexpectedValueException
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testThrowExceptionIfEmailIsNotProvided()
	{
		$attributes = array('payload' => array('email' => null));

		$ticket = m::mock('Google_Auth_LoginTicket')->shouldReceive('getAttributes')->once()->andReturn($attributes)->mock();

		$client = $this->client->shouldReceive('setAccessToken', 'verifyIdToken')->once()->andReturn($ticket)->mock();

		$storage = $this->storage->shouldReceive('get')->once()->with('google');

		$google = new Google($client, $this->storage, $this->config);

		$google->getEmail();
	}

}